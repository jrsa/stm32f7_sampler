#ifndef debugpin_h
#define debugpin_h

#include "stm32f7xx.h"

// PC7 is arduino pin D0 on STM32F769I-DISC board

#define DEBUGPIN_RCC_ENABLE     __HAL_RCC_GPIOC_CLK_ENABLE
#define DEBUGPIN_PORT           GPIOC
#define DEBUGPIN_PIN            GPIO_PIN_7
#define DEBUGPIN_PIN_SET        GPIO_BSRR_BS_7
#define DEBUGPIN_PIN_RESET      GPIO_BSRR_BR_7

#define DEBUGPIN_HIGH           DEBUGPIN_PORT->BSRR = DEBUGPIN_PIN_SET
#define DEBUGPIN_LOW            DEBUGPIN_PORT->BSRR = DEBUGPIN_PIN_RESET


void DebugPin_Init(void);
void DebugPin_Toggle(void);

#endif