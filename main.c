// lovingly copied from cubef7/Projects/STM32F769I-Discovery/Examples/SAI/SAI_AudioPlay/Src/main.c

#include "stm32f7xx_hal.h"

#include "Components/Common/audio.h"
#include "Components/wm8994/wm8994.h"

#include "adc.h"
#include "error.h"
#include "sai.h"

#include "debugpin.h"
#include "serial.h"

#include "audio.h"
#include "audio/simple.h"

// copied from BSP package, for F769I-DISC board
#define AUDIO_I2C_ADDRESS ((uint16_t)0x34)

extern SAI_HandleTypeDef SaiHandle;
AUDIO_DrvTypeDef* audio_drv;

int16_t out_interleaved[DMA_BUFFER_SIZE];
extern struct simple_float samp1;

// prototypes -----------------------------------------------------------------
void SystemClock_Config(void);

// audio callbacks  -----------------------------------------------------------
/*
    sai callbacks (ISR calls into HAL which calls these)
    DMA controller tells us which half of the buffer is currently being read from
    (therefore the other half is complete and we will process it)
*/

void HAL_SAI_TxCpltCallback(SAI_HandleTypeDef* hsai)
{
    // fill first half of DMA buffer
    audio_process(out_interleaved, NULL, DMA_BUFFER_SIZE / 2);
}

void HAL_SAI_TxHalfCpltCallback(SAI_HandleTypeDef* hsai)
{
    // fill second half of buffer here (being half complete, the first half has already been sent out)
    audio_process(out_interleaved + DMA_BUFFER_SIZE / 2, NULL, DMA_BUFFER_SIZE / 2);
}

int main(void)
{
    SCB_EnableICache();
    SCB_EnableDCache();

    HAL_Init();
    SystemClock_Config();

    DebugPin_Init();

    audio_init();

    ADC_Init();
    VCP_UART_Init();

    SAI_Init();

    // the codec has the value 0x8994 at address 0x0, this makes sure that
    // the codec can be seen on i2c and that it is the right part
    if (WM8994_ID != wm8994_drv.ReadID(AUDIO_I2C_ADDRESS)) {
        ERROR();
    }

    audio_drv = &wm8994_drv;
    audio_drv->Reset(AUDIO_I2C_ADDRESS);
    if (0 != audio_drv->Init(AUDIO_I2C_ADDRESS, OUTPUT_DEVICE_HEADPHONE, 0x30, AUDIO_FREQUENCY_44K)) {
        ERROR();
    }

    // this just unmutes the codec, to stop playback it can be muted again
    // before the digital stream is halted to avoid a bad noise
    if (0 != audio_drv->Play(AUDIO_I2C_ADDRESS, NULL, 0)) {
        ERROR();
    }

    // start sending samples to the serial audio interface (SAI) via DMA
    if (HAL_OK != HAL_SAI_Transmit_DMA(&SaiHandle, (uint8_t*)out_interleaved, DMA_BUFFER_SIZE)) {
        ERROR();
    }

    if (HAL_OK != HAL_ADC_Start_DMA(&hAdc, adcBuffer, ADC_BUFFER_SIZE)) {
        ERROR();
    }

    printf("successfully brought up mcu and codec\r\n");

    float ctl = 1.0;

    while (1) {
        ctl = (float)adc_averaged_values[0] / 2048.;
        samp1.fincr = ctl;
    }
}

void SystemClock_Config(void)
{
    RCC_ClkInitTypeDef RCC_ClkInitStruct;
    RCC_OscInitTypeDef RCC_OscInitStruct;
    HAL_StatusTypeDef ret = HAL_OK;

    /* Enable Power Control clock */
    __HAL_RCC_PWR_CLK_ENABLE();

    /* The voltage scaling allows optimizing the power consumption when the device is 
     clocked below the maximum system frequency, to update the voltage scaling value 
     regarding system frequency refer to product datasheet.  */
    __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

    /* Enable HSE Oscillator and activate PLL with HSE as source */
    RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
    RCC_OscInitStruct.HSEState = RCC_HSE_ON;
    RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
    RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
    RCC_OscInitStruct.PLL.PLLM = 25;
    RCC_OscInitStruct.PLL.PLLN = 432;
    RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
    RCC_OscInitStruct.PLL.PLLQ = 9;
    RCC_OscInitStruct.PLL.PLLR = 7;

    ret = HAL_RCC_OscConfig(&RCC_OscInitStruct);
    if (ret != HAL_OK) {
        while (1) {
            ;
        }
    }

    /* Activate the OverDrive to reach the 216 MHz Frequency */
    ret = HAL_PWREx_EnableOverDrive();
    if (ret != HAL_OK) {
        while (1) {
            ;
        }
    }

    /* Select PLL as system clock source and configure the HCLK, PCLK1 and PCLK2 clocks dividers */
    RCC_ClkInitStruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
    RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
    RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
    RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
    RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

    ret = HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_7);
    if (ret != HAL_OK) {
        while (1) {
            ;
        }
    }
}
