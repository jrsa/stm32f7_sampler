#include "error.h"
#include "sai.h"

#include "stm32f7xx_hal.h"

SAI_HandleTypeDef SaiHandle;
DMA_HandleTypeDef hSaiDma;

void SAI_Init(void)
{
    RCC_PeriphCLKInitTypeDef RCC_PeriphCLKInitStruct;

    /* Configure PLLSAI prescalers */
    /* PLLSAI_VCO: VCO_429M 
     SAI_CLK(first level) = PLLSAI_VCO/PLLSAIQ = 429/2 = 214.5 Mhz
     SAI_CLK_x = SAI_CLK(first level)/PLLSAIDIVQ = 214.5/19 = 11.289 Mhz */
    RCC_PeriphCLKInitStruct.PeriphClockSelection = RCC_PERIPHCLK_SAI2;
    RCC_PeriphCLKInitStruct.Sai2ClockSelection = RCC_SAI2CLKSOURCE_PLLSAI;
    RCC_PeriphCLKInitStruct.PLLSAI.PLLSAIN = 429;
    RCC_PeriphCLKInitStruct.PLLSAI.PLLSAIQ = 2;
    RCC_PeriphCLKInitStruct.PLLSAIDivQ = 19;

    if (HAL_RCCEx_PeriphCLKConfig(&RCC_PeriphCLKInitStruct) != HAL_OK) {
        ERROR();
    }

    /* Initialize SAI */
    __HAL_SAI_RESET_HANDLE_STATE(&SaiHandle);

    SaiHandle.Instance = AUDIO_SAIx;

    __HAL_SAI_DISABLE(&SaiHandle);

    SaiHandle.Init.AudioMode = SAI_MODEMASTER_TX;
    SaiHandle.Init.Synchro = SAI_ASYNCHRONOUS;
    SaiHandle.Init.OutputDrive = SAI_OUTPUTDRIVE_ENABLE;
    SaiHandle.Init.NoDivider = SAI_MASTERDIVIDER_ENABLE;
    SaiHandle.Init.FIFOThreshold = SAI_FIFOTHRESHOLD_1QF;
    SaiHandle.Init.AudioFrequency = SAI_AUDIO_FREQUENCY_44K;
    SaiHandle.Init.Protocol = SAI_FREE_PROTOCOL;
    SaiHandle.Init.DataSize = SAI_DATASIZE_16;
    SaiHandle.Init.FirstBit = SAI_FIRSTBIT_MSB;
    SaiHandle.Init.ClockStrobing = SAI_CLOCKSTROBING_FALLINGEDGE;

    SaiHandle.FrameInit.FrameLength = 32;
    SaiHandle.FrameInit.ActiveFrameLength = 16;
    SaiHandle.FrameInit.FSDefinition = SAI_FS_CHANNEL_IDENTIFICATION;
    SaiHandle.FrameInit.FSPolarity = SAI_FS_ACTIVE_LOW;
    SaiHandle.FrameInit.FSOffset = SAI_FS_BEFOREFIRSTBIT;

    SaiHandle.SlotInit.FirstBitOffset = 0;
    SaiHandle.SlotInit.SlotSize = SAI_SLOTSIZE_DATASIZE;
    SaiHandle.SlotInit.SlotNumber = 2;
    SaiHandle.SlotInit.SlotActive = (SAI_SLOTACTIVE_0 | SAI_SLOTACTIVE_1);

    if (HAL_OK != HAL_SAI_Init(&SaiHandle)) {
        ERROR();
    }

    /* Enable SAI to generate clock used by audio driver */
    // this is a macro that just sets the enable bit in a SAI control register
    // (SAI->CR1 |= SAI_xCR1_SAIEN)
    __HAL_SAI_ENABLE(&SaiHandle);

    /* Initialize audio driver */


}

void HAL_SAI_MspInit(SAI_HandleTypeDef* hsai)
{
    GPIO_InitTypeDef GPIO_Init;

    /* Enable SAI1 clock */
    __HAL_RCC_SAI1_CLK_ENABLE();

    /* Configure GPIOs used for SAI2 */
    AUDIO_SAIx_MCLK_ENABLE();
    AUDIO_SAIx_SCK_ENABLE();
    AUDIO_SAIx_FS_ENABLE();
    AUDIO_SAIx_SD_ENABLE();

    GPIO_Init.Mode = GPIO_MODE_AF_PP;
    GPIO_Init.Pull = GPIO_NOPULL;
    GPIO_Init.Speed = GPIO_SPEED_FREQ_VERY_HIGH;

    GPIO_Init.Alternate = AUDIO_SAIx_FS_AF;
    GPIO_Init.Pin = AUDIO_SAIx_FS_PIN;
    HAL_GPIO_Init(AUDIO_SAIx_FS_GPIO_PORT, &GPIO_Init);

    GPIO_Init.Alternate = AUDIO_SAIx_SCK_AF;
    GPIO_Init.Pin = AUDIO_SAIx_SCK_PIN;
    HAL_GPIO_Init(AUDIO_SAIx_SCK_GPIO_PORT, &GPIO_Init);

    GPIO_Init.Alternate = AUDIO_SAIx_SD_AF;
    GPIO_Init.Pin = AUDIO_SAIx_SD_PIN;
    HAL_GPIO_Init(AUDIO_SAIx_SD_GPIO_PORT, &GPIO_Init);

    GPIO_Init.Alternate = AUDIO_SAIx_MCLK_AF;
    GPIO_Init.Pin = AUDIO_SAIx_MCLK_PIN;
    HAL_GPIO_Init(AUDIO_SAIx_MCLK_GPIO_PORT, &GPIO_Init);

    /* Configure DMA used for SAI2 */
    __HAL_RCC_DMA2_CLK_ENABLE();

    if (hsai->Instance == AUDIO_SAIx) {
        hSaiDma.Init.Channel = DMA_CHANNEL_10;
        hSaiDma.Init.Direction = DMA_MEMORY_TO_PERIPH;
        hSaiDma.Init.PeriphInc = DMA_PINC_DISABLE;
        hSaiDma.Init.MemInc = DMA_MINC_ENABLE;
        hSaiDma.Init.PeriphDataAlignment = DMA_PDATAALIGN_HALFWORD;
        hSaiDma.Init.MemDataAlignment = DMA_MDATAALIGN_HALFWORD;
        hSaiDma.Init.Mode = DMA_CIRCULAR;
        hSaiDma.Init.Priority = DMA_PRIORITY_HIGH;
        hSaiDma.Init.FIFOMode = DMA_FIFOMODE_ENABLE;
        hSaiDma.Init.FIFOThreshold = DMA_FIFO_THRESHOLD_FULL;
        hSaiDma.Init.MemBurst = DMA_MBURST_SINGLE;
        hSaiDma.Init.PeriphBurst = DMA_PBURST_SINGLE;

        /* Select the DMA instance to be used for the transfer : DMA2_Stream6 */
        hSaiDma.Instance = DMA2_Stream6;

        /* Associate the DMA handle */
        __HAL_LINKDMA(hsai, hdmatx, hSaiDma);

        /* Deinitialize the Stream for new transfer */
        HAL_DMA_DeInit(&hSaiDma);

        /* Configure the DMA Stream */
        if (HAL_OK != HAL_DMA_Init(&hSaiDma)) {
            ERROR();
        }
    }

    HAL_NVIC_SetPriority(DMA2_Stream6_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(DMA2_Stream6_IRQn);
}
