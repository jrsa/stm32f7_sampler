#ifndef adc_h
#define adc_h

/*
 * A0   PA6     ADC1_IN6    ADC2_IN6
 * A1   PA4     ADC1_IN4    ADC2_IN4
 * A2   PC5     ADC1_IN15   ADC2_IN15
 * 
 * A3   PF10    ADC3_IN8
 * A4   PF8     ADC3_IN6
 * A5   PF9     ADC3_IN7

*/

#define ADC_BUFFER_SIZE 16

extern ADC_HandleTypeDef hAdc;
extern uint32_t adcBuffer[];
extern uint16_t adc_averaged_values[];

void ADC_Init(void);

#endif