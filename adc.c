#include <stm32f7xx_hal.h>

#include "adc.h"

// FOR DEBUG LED ONLY
#include "STM32F769I-Discovery/stm32f769i_discovery.h"


/* 
 * note: only ADC_CHANNEL_COUNT channels will be configured in ADC_Init,
 * regardless of the total number of channels in the `channels` array
 */
#define ADC_CHANNEL_COUNT 3
ADC_ChannelConfTypeDef channels[] = {
    {
        .Channel = ADC_CHANNEL_12,
        .Rank = ADC_REGULAR_RANK_1,
        .SamplingTime = ADC_SAMPLETIME_15CYCLES
    },
    {
        .Channel = ADC_CHANNEL_6,
        .Rank = ADC_REGULAR_RANK_2,
        .SamplingTime = ADC_SAMPLETIME_15CYCLES
    },
    {
        .Channel = ADC_CHANNEL_4,
        .Rank = ADC_REGULAR_RANK_3,
        .SamplingTime = ADC_SAMPLETIME_15CYCLES
    },
};

ADC_HandleTypeDef hAdc;

uint32_t adcBuffer[ADC_BUFFER_SIZE] = {0,};
uint16_t adc_averaged_values[ADC_CHANNEL_COUNT] = {0,};

void ADC_Init(void)
{
    ADC_ChannelConfTypeDef sConfig;

    /* Configure the ADC peripheral */
    hAdc.Instance = ADC1;

    hAdc.Init.ClockPrescaler = ADC_CLOCKPRESCALER_PCLK_DIV2;
    hAdc.Init.Resolution = ADC_RESOLUTION_12B;
    hAdc.Init.ScanConvMode = ENABLE;
    hAdc.Init.ContinuousConvMode = ENABLE;
    hAdc.Init.DiscontinuousConvMode = DISABLE;
    hAdc.Init.NbrOfDiscConversion = 0;
    hAdc.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
    hAdc.Init.ExternalTrigConv = ADC_EXTERNALTRIGCONV_T1_CC1;
    hAdc.Init.DataAlign = ADC_DATAALIGN_RIGHT;
    hAdc.Init.NbrOfConversion = ADC_CHANNEL_COUNT;
    hAdc.Init.DMAContinuousRequests = ENABLE;
    hAdc.Init.EOCSelection = DISABLE;

    if (HAL_ADC_Init(&hAdc) != HAL_OK) {
        while (1)
            ;
    }

    for (int i = 0; i < ADC_CHANNEL_COUNT; i++) {
        if (HAL_ADC_ConfigChannel(&hAdc, &channels[i]) != HAL_OK) {
            while (1)
                ;
        }
    }
}

void HAL_ADC_MspInit(ADC_HandleTypeDef* hadc)
{
    static DMA_HandleTypeDef hdma_adc;
    GPIO_InitTypeDef gpio_init;

    __HAL_RCC_ADC1_CLK_ENABLE();
    __HAL_RCC_DMA2_CLK_ENABLE();

    // gpios wired to analog arduino pins A0, A1, A2
    __HAL_RCC_GPIOA_CLK_ENABLE();
    __HAL_RCC_GPIOC_CLK_ENABLE();

    gpio_init.Pin = GPIO_PIN_4 | GPIO_PIN_6;
    gpio_init.Mode = GPIO_MODE_ANALOG;
    gpio_init.Pull = GPIO_NOPULL;
    gpio_init.Speed = GPIO_SPEED_LOW;
    HAL_GPIO_Init(GPIOA, &gpio_init);

    gpio_init.Pin = GPIO_PIN_2;
    HAL_GPIO_Init(GPIOC, &gpio_init);

    hdma_adc.Instance = DMA2_Stream0;

    hdma_adc.Init.Channel = DMA_CHANNEL_0;
    hdma_adc.Init.Direction = DMA_PERIPH_TO_MEMORY;
    hdma_adc.Init.PeriphInc = DMA_PINC_DISABLE;
    hdma_adc.Init.MemInc = DMA_MINC_ENABLE;
    hdma_adc.Init.PeriphDataAlignment = DMA_PDATAALIGN_WORD;
    hdma_adc.Init.MemDataAlignment = DMA_MDATAALIGN_WORD;
    hdma_adc.Init.Mode = DMA_CIRCULAR;
    hdma_adc.Init.Priority = DMA_PRIORITY_HIGH;
    hdma_adc.Init.FIFOMode = DMA_FIFOMODE_DISABLE;
    hdma_adc.Init.FIFOThreshold = DMA_FIFO_THRESHOLD_HALFFULL;
    hdma_adc.Init.MemBurst = DMA_MBURST_SINGLE;
    hdma_adc.Init.PeriphBurst = DMA_PBURST_SINGLE;

    HAL_DMA_Init(&hdma_adc);

    __HAL_LINKDMA(hadc, DMA_Handle, hdma_adc);

    HAL_NVIC_SetPriority(DMA2_Stream0_IRQn, 1, 0);
    HAL_NVIC_EnableIRQ(DMA2_Stream0_IRQn);
}

void HAL_ADC_MspDeInit(ADC_HandleTypeDef* hadc)
{
    __HAL_RCC_ADC_FORCE_RESET();
    __HAL_RCC_ADC_RELEASE_RESET();
}

static int chan_idx = 0;
void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* AdcHandle)
{
    for (int i = 0; i < ADC_BUFFER_SIZE; i++) {
        // TODO: this obviously doesn't do any averaging at all
        adc_averaged_values[chan_idx] = adcBuffer[i];
        chan_idx++;

        if (chan_idx >= ADC_CHANNEL_COUNT) {
            chan_idx = 0;
        }
    }
}
