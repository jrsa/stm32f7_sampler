#ifndef audio_h
#define audio_h

#define DMA_BUFFER_SIZE 16

#include <stdint.h>

void audio_init(void);
void audio_process(uint16_t *in, uint16_t *out, uint32_t size);

#endif