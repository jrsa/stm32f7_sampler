#include "debugpin.h"

#include "stm32f7xx_hal.h"

void DebugPin_Init(void)
{
    GPIO_InitTypeDef init;

    DEBUGPIN_RCC_ENABLE();

    init.Pin   = DEBUGPIN_PIN;
    init.Mode  = GPIO_MODE_OUTPUT_PP;
    init.Pull  = GPIO_PULLUP;
    init.Speed = GPIO_SPEED_HIGH;
    
    HAL_GPIO_Init(DEBUGPIN_PORT, &init);
}

void DebugPin_Toggle(void)
{
    HAL_GPIO_TogglePin(DEBUGPIN_PORT, DEBUGPIN_PIN);
}