#ifndef audio_interleave_h
#define audio_interleave_h

#include <stdint.h>

/*
 * copies the contents of a buffer containing interleaved samples (LRLRLRLRLR)
 * into two separate buffers, one for left and one for right
 */
void audio_deinterleave_n(int16_t *ldst, int16_t *rdst, int16_t *src, uint32_t sz);


/*
 * copies the contents of a left and right buffer into one interleaved buffer
 */
void audio_interleave_n(int16_t *dst, int16_t *lsrc, int16_t *rsrc, uint32_t sz);

#endif