#include "simple.h"
// #include "util.h"

#define ARM_MATH_CM7
#include "arm_math.h"

int16_t next(struct simple* buf)
{
    buf->pos += buf->incr;

    if (buf->pos > buf->len) {
        buf->pos = 0;
    }

    return buf->data[buf->pos];
}

int16_t next_interp(struct simple_float* buf)
{
    double wholePos = 0.0, x1 = 0.0, x2 = 0.0;
    uint32_t index;

    buf->fpos += buf->fincr;

    if (buf->fpos > buf->len) {
        buf->fpos = 0.0;
    }

    double fract = modf(buf->fpos, &wholePos);
    index = (uint32_t)wholePos;

    x1 = (double)buf->data[index];
    x2 = (double)buf->data[index + 1];

    return (int16_t)(x1 * (1 - fract)) + (x2 * fract);
}

void next_n(int16_t* dst, uint32_t sz, struct simple* buf)
{
    while (sz--) {
        buf->pos += buf->incr;

        if (buf->pos > buf->len)
            buf->pos = 0;

        *dst++ = buf->data[buf->pos];
    }
}

void next_interp_n(int16_t* dst, uint32_t sz, struct simple_float* buf)
{
    double wholePos = 0.0, x1 = 0.0, x2 = 0.0;
    uint32_t index;
    while (sz--) {
        buf->fpos += buf->fincr;

        if (buf->fpos > buf->len) {
            buf->fpos = 0.0;
        }

        double fract = modf(buf->fpos, &wholePos);
        index = (uint32_t)wholePos;

        x1 = (double)buf->data[index];
        x2 = (double)buf->data[index + 1];

        *dst++ = (int16_t)(x1 * (1 - fract)) + (x2 * fract);
    }
}
