#include "interleave.h"

void audio_deinterleave_n(int16_t *ldst, int16_t *rdst, int16_t *src, uint32_t sz)
{
    while (sz) {
        *ldst++ = *src++;
        sz--;
        *rdst++ = *src++;
        sz--;
    }
}

void audio_interleave_n(int16_t *dst, int16_t *lsrc, int16_t *rsrc, uint32_t sz)
{
    while (sz) {
        *dst++ = *lsrc++;
        sz--;
        *dst++ = *rsrc++;
        sz--;
    }
}