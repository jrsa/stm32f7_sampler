#ifndef simple_h
#define simple_h

#include <stdint.h>

// simple buffer

struct simple {
    int16_t *data;
    uint32_t len;

    uint32_t pos;
    uint32_t incr;
};

struct simple_float {
    int16_t* data;
    uint32_t len;

    float fpos;
    float fincr;
};


int16_t next(struct simple* buf);
int16_t next_interp(struct simple_float* buf);

void next_n(int16_t *dst, uint32_t size, struct simple* buf);
void next_interp_n(int16_t *dst, uint32_t size, struct simple_float* buf);

#endif
