#include "debugpin.h"

#include "stm32f7xx_hal.h"

extern SAI_HandleTypeDef SaiHandle;
extern ADC_HandleTypeDef hAdc;


void SysTick_Handler(void)
{
    HAL_IncTick();
}

void DMA2_Stream6_IRQHandler(void)
{
    HAL_DMA_IRQHandler(SaiHandle.hdmatx);
}

void DMA2_Stream0_IRQHandler(void)
{
    HAL_DMA_IRQHandler(hAdc.DMA_Handle);
}