#ifndef error_h
#define error_h

#define ERROR() Error_Handler(__FILE__, __LINE__)

void Error_Handler(char* file, int line);

#endif