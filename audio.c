#include "audio.h"

#include "audio/interleave.h"
#include "audio/simple.h"

int16_t out_right[DMA_BUFFER_SIZE / 2];
int16_t out_left[DMA_BUFFER_SIZE / 2];

// write the sample files with the command: st-flash write <file> <address>
// take care not to overwrite any of the firmware code
struct simple_float samp1 = {
    .data = (int16_t*)0x08080000,
    .len = 77389,
    .fpos = 0.0,
    .fincr = 1.3
};

struct simple samp2 = {
    .data = (int16_t *)0x08080000,
    .len = 77389,
    .incr = 1
};

void audio_init(void)
{
}

void audio_process(uint16_t *out, uint16_t *in, uint32_t size)
{
    next_interp_n(out_left, size / 2, &samp1);
    next_n(out_right, size / 2, &samp2);
    audio_interleave_n(out, out_left, out_right, size);
}
