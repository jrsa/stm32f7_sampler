#include <stm32f7xx_hal.h>

/*  UART connected to st-link, for serial communication with PC
 *  to send: HAL_UART_Transmit(&vcp_uart, data, 6, HAL_MAX_DELAY)
 */

UART_HandleTypeDef vcp_uart;
void VCP_UART_Init(void)
{
    vcp_uart.Instance = USART1;
    vcp_uart.Init.BaudRate = 115200;
    vcp_uart.Init.WordLength = UART_WORDLENGTH_8B;
    vcp_uart.Init.StopBits = UART_STOPBITS_1;
    vcp_uart.Init.Parity = UART_PARITY_NONE;
    vcp_uart.Init.Mode = UART_MODE_TX;
    vcp_uart.Init.HwFlowCtl = UART_HWCONTROL_NONE;
    vcp_uart.Init.OverSampling = UART_OVERSAMPLING_16;
    HAL_UART_Init(&vcp_uart);
}

/*  "callback" from inside HAL_UART_Init, 
 *  this is where we set up the IO for the UART */
void HAL_UART_MspInit(UART_HandleTypeDef* huart)
{
    GPIO_InitTypeDef gpioa_init;
    if (huart->Instance == USART1) {
        __HAL_RCC_USART1_CLK_ENABLE();
        __HAL_RCC_GPIOA_CLK_ENABLE();

        gpioa_init.Pin = GPIO_PIN_9 | GPIO_PIN_10;
        gpioa_init.Mode = GPIO_MODE_AF_PP;
        gpioa_init.Pull = GPIO_NOPULL;
        gpioa_init.Speed = GPIO_SPEED_LOW;
        gpioa_init.Alternate = GPIO_AF7_USART1;
        HAL_GPIO_Init(GPIOA, &gpioa_init);
    }
}

int __io_putchar(int ch) {
    uint8_t data = (uint8_t)ch;
    HAL_UART_Transmit(&vcp_uart, &data, 1, HAL_MAX_DELAY);
}